 <div class="deznav">
    <div class="deznav-scroll">
		<ul class="metismenu" id="menu">
            <li><a href="dashboard" class="ai-icon" aria-expanded="false">
                    <i class="flaticon-381-home"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
					<i class="flaticon-381-folder"></i>
					<span class="nav-text">Master Data</span>
				</a>
                <ul aria-expanded="false">
                    <li><a href="customer">Customer</a></li>
                    <li><a href="supplier">Supplier</a></li>
                    <li><a href="layanan">Layanan</a></li>
                    <li><a href="barang">Barang</a></li>
				</ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="nav-text">Transaksi</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="transaksi_supplier">Supplier</a></li>
                    <li><a href="transaksi_customer">Customer</a></li>
                </ul>
            </li>
             <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="fa fa-industry"></i>
                    <span class="nav-text">Produksi</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="transaksi_baru">Baru</a></li>
                    <li><a href="transaksi_proses">Proses</a></li>
                    <li><a href="transaksi_selesai">Selesai</a></li>
                </ul>
            </li>
            <li><a class="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
                    <i class="flaticon-381-list"></i>
                    <span class="nav-text">Report</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="report_perbulan">Penjualan</a></li>
                </ul>
            </li>
             <li><a href="logout" class="ai-icon" aria-expanded="false">
                    <i class="fa fa-sign-out"></i>
                    <span class="nav-text">Logout</span>
                </a>
            </li>
        </ul>
		<div class="copyright">
			<center><strong>Percatakan Omah Banner</strong> © 2022 All Rights Reserved</center>
		</div>
	</div>
</div>