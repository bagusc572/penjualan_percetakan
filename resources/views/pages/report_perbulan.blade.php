@extends('../main')
@section('container')

<?php 

if (isset($_REQUEST['hapus'])) {
	$transaksi_id = $_REQUEST['transaksi_id'];

	$act = DB::DELETE((DB::raw("
		DELETE FROM `m_transaksi_customer` WHERE `transaksi_id`='".$transaksi_id."'
		"))); 

	if ($act) {
		echo '
		<div class="alert alert-success">
		<strong>Perhatian !</strong> Data berhasil Dihapus
		</div>
		';
	}else{
		echo '
		<div class="alert alert-danger">
		<strong>Perhatian !</strong> Data gagal Dihapus
		</div>
		';
	}
}

?>
<div class="row">
	<div class="col-xl-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<form action="" method="POST">
					{{ @csrf_field() }}
					<table style="width: 50%">
						<tr>
							<td>
								<label>Mulai Tanggal</label><br>
								<input type="date" name="tgl_awal" class="form-control" placeholder="Tanggal Awal" required="">
							</td>
							<td>
								<label>Sampai Tanggal</label><br>
								<input type="date" name="tgl_akhir" class="form-control" placeholder="Tanggal Akhir" required=""></td>
								<td><label>-</label><br><button class="btn btn-sm btn-danger">Cari</button></td>
							</tr>
						</table>
					</div>
				</form>
				<div class="card-body">
					<table class="display min-w850" id="example">
						<thead>    
							<tr>
								<th width="1%">#</th>
								<th width="10%">Kode Transaksi</th>
								<th width="10%">Tanggal</th>
								<th>Barang</th>
								<th>customer</th>
								<th>Harga</th>
								<th>Quantity</th>
								<th>Total</th>
								<th>Tunai</th>
								<th>Kembalian</th>
								<th>status</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no=1;
							$show_transaksi_customer = DB::SELECT(DB::RAW("
								SELECT * FROM `m_transaksi_customer` AS a
								LEFT JOIN m_layanan as b ON a.layanan_id = b.layanan_id
								LEFT JOIN m_customer as c ON a.customer_id = c.customer_id
								ORDER BY a.create_at DESC
								"));
							foreach ($show_transaksi_customer as $datransaksi_customer):
								?>
								<tr>
									<td><?php echo $no++; ?></td>        
									<td><?php echo $datransaksi_customer->kode; ?></td>   
									<td><?php echo $datransaksi_customer->tgl_pemesanan; ?></td>   
									<td><?php echo $datransaksi_customer->nama_layanan; ?></td>   
									<td><?php echo $datransaksi_customer->nama_customer; ?></td>   
									<td><?php echo number_format($datransaksi_customer->harga); ?></td>   
									<td><?php echo $datransaksi_customer->qty; ?></td>   
									<td style="text-align: center;"><?php echo number_format($datransaksi_customer->total); ?></td>   
									<td style="text-align: center;"><?php echo number_format($datransaksi_customer->tunai); ?></td>   
									<td style="text-align: center;"><?php echo number_format($datransaksi_customer->kembalian); ?></td>   
									<td><?php echo $datransaksi_customer->status_pesanan; ?></td>   

								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7">Total Transaksi</td>
								<td>10000</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	@endsection