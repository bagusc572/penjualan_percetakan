@extends('../main')
@section('container')
<style type="text/css">
	.form-control{
		color: black;
	}
</style>
<div class="row">
	<div class="col-xl-5 col-lg-5">
		<?php 
		if (isset($_REQUEST['add_cart'])) {
			$layanan_id = $_REQUEST['layanan_id'];
			$quantity = $_REQUEST['quantity'];
			$harga = $_REQUEST['harga'];
			$total = $_REQUEST['harga'];

			$act = DB::INSERT((DB::raw("
				INSERT INTO `m_transaksi_customer_temporary`(
				`layanan_id`,
				`harga`,
				`qty`,
				`total`) VALUES (
				'".$layanan_id."',
				'".$harga."',
				'".$quantity."',
				'".$total."')
				"))); 

		}

		if (isset($_REQUEST['hapus'])) {
			$layanan_id = $_REQUEST['layanan_id'];

			$act = DB::INSERT((DB::raw("
				DELETE FROM `m_transaksi_customer_temporary` WHERE layanan_id='".$layanan_id."'
				"))); 

		}

		if (isset($_REQUEST['set_qty'])) {
			$qty = $_REQUEST['set_qty'];
			$layanan_id = $_REQUEST['layanan_id'];
			$harga = $_REQUEST['harga'];
			$act = DB::update((DB::raw("
				UPDATE m_transaksi_customer_temporary SET qty='".$qty."' WHERE layanan_id='".$layanan_id."'
				"))); 

		}

		if (isset($_REQUEST['simpan'])) {
			$kode_transaksi = $_REQUEST['kode_transaksi'];
			$keranjang = DB::SELECT(DB::RAW("
				SELECT 
				a.layanan_id,
				b.nama_layanan,
				a.qty as qty,
				a.harga as harga 
				FROM `m_transaksi_customer_temporary` as a
				LEFT JOIN `m_layanan` as b ON a.layanan_id = b.layanan_id
				GROUP BY a.layanan_id, b.nama_layanan, a.harga, a.qty
			"));

			$total = DB::SELECT(DB::RAW("SELECT sum(qty*harga) as tot FROM ( SELECT a.layanan_id, b.nama_layanan, SUM(a.qty) as qty, SUM(a.harga) as harga FROM `m_transaksi_customer_temporary` as a LEFT JOIN `m_layanan` as b ON a.layanan_id = b.layanan_id GROUP BY a.layanan_id, b.nama_layanan ) as v
											"));
			foreach ($keranjang as $data):

				$tgl_pemesanan = $_REQUEST['tgl_pemesanan'];
				$kode_transaksi = $_REQUEST['kode_transaksi'];
				$layanan_id = $data->layanan_id;
				$customer_id = $_REQUEST['customer_id'];
				$quantity = $data->qty;
				$harga = $data->harga;
				$harga_total = $total[0]->tot;
				$tunai = $_REQUEST['tunai'];
				$kembalian = $_REQUEST['kembalian'];
				$keterangan = $_REQUEST['keterangan'];
				$status_pembayaran = $_REQUEST['status_pembayaran'];

	
				$act = DB::INSERT((DB::raw("
				INSERT INTO `m_transaksi_customer`(
				 `kode_transaksi`,
				 `tgl_pemesanan`,
				 `layanan_id`,
				 `customer_id`,
				 `qty`,
				 `keterangan`,
				 `create_at`,
				 `harga`,
				 `total`,
				 `tunai`,
				 `kembalian`,
				 `status_pesanan`,
				 `status_pembayaran`
				 ) VALUES (
				 '".$kode_transaksi."',
				 '".$tgl_pemesanan."',
				 '".$layanan_id."',
				 '".$customer_id."',
				 '".$quantity."',
				 '".$keterangan."',
				 '".date('Y-m-d H:i:s')."',
				 '".$harga."',
				 '".$harga_total."',
				 '".$tunai."',
				 '".$kembalian."',
				 'baru',
				 '".$status_pembayaran."'
				 )
				"))); 
			endforeach;

			if ($act) {
				echo  "<script>
					document.location='./print?kode_transaksi=".$kode_transaksi."'
				</script>";
			}

		}


		?>
		
		<div class="card">
			<div class="card-header bg-primary">
				<h4 class="card-title" style="color: white">Layanan</h4>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered" style="width: 100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Layanan</th>
							<th>Harga</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$no =1;
						$show_layanan = DB::SELECT(DB::RAW("SELECT * FROM `m_layanan` ORDER BY create_at ASC"));
						foreach ($show_layanan as $data):

							$bb = DB::SELECT(DB::RAW("SELECT * FROM `m_transaksi_customer_temporary` 
													 WHERE layanan_id='".$data->layanan_id."'
													 ORDER BY layanan_id ASC LIMIT 1"));
						
						?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $data->nama_layanan; ?></td>
								<td><?php echo number_format($data->harga,2); ?></td>
								<td>
									<?php if(empty($bb)) : ?>
									<form action="" method="POST">
										{{ csrf_field() }}
										<input type="hidden" name="layanan_id" value="<?php echo $data->layanan_id; ?>">
										<input type="hidden" name="harga" value="<?php echo $data->harga; ?>">
										<input type="hidden" name="total" value="<?php echo $data->harga; ?>">
										<input type="hidden" name="quantity" value="1">
										<button type="submit" name="add_cart" class="btn btn-outline-primary btn-sm"><i class="fa fa-shopping-cart"></i></button>
									</form>
									<?php else : ?>
										<span style="color: green">Sudah Dipilih</span>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<form action="" method="POST">
					{{ csrf_field() }}
				</form>
			</div>
		</div>
	</div>
	<div class="col-xl-7 col-lg-7">
		<div class="card">
			<div class="card-header bg-primary">
				<h4 class="card-title" style="color: white">List Order</h4>
			</div>
			<div class="card-body">
				<div class="table table-responsive">
					
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Layanan</th>
								<th>Qty</th>
								<th>Harga</th>
								<th>Total</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no =1;
							$show_layanan_temp = DB::SELECT(DB::RAW("SELECT 
								a.layanan_id,
								b.nama_layanan,
								a.qty as qty,
								a.harga as harga 
								FROM `m_transaksi_customer_temporary` as a
								LEFT JOIN `m_layanan` as b ON a.layanan_id = b.layanan_id
								GROUP BY a.layanan_id, b.nama_layanan, a.harga, a.qty
								"));
								foreach ($show_layanan_temp as $data):?>

									<tr>
										<td><?php echo $no++; ?></td>
										<td style="text-align: left;"><?php echo $data->nama_layanan; ?></td>
										<td nowrap="">
											<form action="" method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="layanan_id" value="<?php echo $data->layanan_id; ?>">
												<input type="hidden" name="harga" value="<?php echo $data->harga; ?>">
												<select onchange="form.submit()" name="set_qty">
													<?php for ($i=1; $i <=10 ; $i++):?>
														<option value="<?php echo $i; ?>"
															<?php 
																echo ($i==$data->qty) ? 'selected' : '';
															?>
															><?php echo $i; ?></option>
													<?php endfor; ?>
												</select>
											</form>
											
										</td>
										<td><?php echo number_format($data->harga); ?></td>
										<td><?php echo number_format($data->harga*$data->qty); ?></td>
										<td>
											<form accept="" method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="layanan_id" value="<?php echo $data->layanan_id; ?>">
												<button type="submit" onclick="return confirm('Apakah anda ingin menghapus data ini ?');" name="hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
											</form>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4" style="background-color: grey; color: white">Total</td>
									<td style="text-align: right;">
										<?php 
										$total = DB::SELECT(DB::RAW("SELECT sum(qty*harga) as tot FROM ( SELECT a.layanan_id, b.nama_layanan, SUM(a.qty) as qty, SUM(a.harga) as harga FROM `m_transaksi_customer_temporary` as a LEFT JOIN `m_layanan` as b ON a.layanan_id = b.layanan_id GROUP BY a.layanan_id, b.nama_layanan ) as v
											"));

										echo number_format($total[0]->tot);
										?>
										<input type="hidden" id="harga_total" value="<?php echo $total[0]->tot; ?>">
									</td>
									<td style="background-color: grey; color: white"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-12 col-lg-12">
			<div class="card">
				<div class="card-body">

					<form action="" method="POST">
						{{ @csrf_field() }}
					<div class="form-group">
						<label>Kode Transaksi</label>
						<input type="text" readonly class="form-control input-default" name="kode_transaksi" value="KD<?php echo date('Ymdhis'); ?>">
					</div>
					<div class="form-group">
						<label>Tanggal</label>
						<input type="date" readonly class="form-control input-default" name="tgl_pemesanan" value="<?php echo date('Y-m-d'); ?>">
					</div>
					<div class="form-group">
						<label>Pilih Customer</label>
						<select class="form-control" name="customer_id">
							<?php 
							$show_customer = DB::SELECT(DB::RAW("SELECT * FROM `m_customer` ORDER BY create_at ASC"));
							foreach ($show_customer as $data):
								?>
								<option value="<?php echo $data->customer_id; ?>"><?php echo strtoupper($data->nama_customer); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Tunai</label>
						<input type="number" onkeyup="cash();"  id="tunai" class="form-control input-default" name="tunai" placeholder="Tunai">
					</div>
					<div class="form-group">
						<label>Kembalian</label>
						<input type="number" id="kembalian" class="form-control input-default" name="kembalian" placeholder="Kembalian">
					</div>
					<div class="form-group">
						<label>Pembayaran</label>
						<select class="form-control" name="status_pembayaran">
							<option value="Lunas">Lunas</option>
							<option value="DP">DP</option>
						</select>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<input type="text" class="form-control input-default" name="keterangan" placeholder="keterangan">
					</div>
					<div class="form-group">
						<a href="transaksi_customer" class="btn btn-danger mb-2">
							<i class="fa fa-arrow"></i> Kembali
						</a>
						<button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	function cek_harga() {
		var layanan_id = $("#layanan_id").val();
		$.ajax({
			url: 'cek_harga_layanan?layanan_id='+layanan_id,
			type: 'GET',
			dataType: 'html',
			success : function(data) {
				$("#harga").val(data);
			}
		})
		
	}
	function total_harga() {
		var harga = parseInt($("#harga").val());
		var quantity = parseInt($("#quantity").val());
		$("#harga_total").val(harga*quantity);
		
	}

	function  cash() {
		var tunai = parseInt($("#tunai").val());
		var harga_total = parseInt($("#harga_total").val());
		$("#kembalian").val(tunai-harga_total);
	}


	
</script>

@endsection