@extends('../main')
@section('container')

<div class="row">
	<div class="col-xl-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<!-- <h4 class="card-title">Data Customer</h4> -->
				<a href="add_transaksi_supplier" class="btn btn-primary mb-2">
					<i class="fa fa-shopping-cart"></i> Tambah transaksi supplier
				</a>
			</div>
			<div class="card-body">
				<table class="display min-w850" id="example">
					<thead>    
						<tr>
							<th width="1%">#</th>
							<th width="10%">Tanggal</th>
							<th>Barang</th>
							<th>Supplier</th>
							<th>Quantity</th>
							<th>Harga</th>
							<th>Total</th>
							<th>Keterangan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$no=1;
						$show_transaksi_supplier = DB::SELECT(DB::RAW("
							SELECT a.*, b.nama_supplier, c.nama_barang FROM m_transaksi_supplier as a 
							LEFT JOIN m_supplier as b ON a.supplier_id = b.supplier_id
							LEFT JOIN m_barang as c ON a.barang_id = c.barang_id
							ORDER BY a.create_at DESC"));
						foreach ($show_transaksi_supplier as $datransaksi_supplier):
							?>
							<tr>
								<td><?php echo $no++; ?></td>        
								<td><?php echo $datransaksi_supplier->tgl_pemesanan; ?></td>   
								<td><?php echo $datransaksi_supplier->nama_barang; ?></td>   
								<td><?php echo $datransaksi_supplier->nama_supplier; ?></td>   
								<td><?php echo $datransaksi_supplier->qty; ?></td>   
								<td><?php echo number_format($datransaksi_supplier->harga_beli); ?></td>   
								<td style="text-align: center;">
									<?php echo number_format($datransaksi_supplier->harga_beli*$datransaksi_supplier->qty); ?>		
								</td>   
								<td><?php echo $datransaksi_supplier->keterangan; ?></td>   
								<td>
									<form action="" method="POST">
										{{ csrf_field() }}
										<a href="edit_transaksi_supplier?id_transaksi=<?php echo $datransaksi_supplier->transaksi_id; ?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>

										<input type="hidden" name="transaksi_id" value="<?php echo $datransaksi_supplier->transaksi_id; ?>">
										<button onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" class="btn btn-danger btn-sm" type="submit" name="hapus"><i class="fa fa-trash"></i></button>
									</form>
									
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection