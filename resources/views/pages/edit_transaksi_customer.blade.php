@extends('../main')
@section('container')
<style type="text/css">
	.form-control{
		color: black;
	}
</style>
<div class="row">
	<div class="col-xl-7 col-lg-7">
		<?php 
		$datcus = DB::SELECT(DB::RAW("SELECT * FROM m_transaksi_customer WHERE transaksi_id='".$_REQUEST['id_transaksi']."'"));
		if (isset($_REQUEST['update'])) {

			$transaksi_id = $_REQUEST['id_transaksi'];
			$tgl_pemesanan = $_REQUEST['tgl_pemesanan'];
			$kode_transaksi = $_REQUEST['kode_transaksi'];
			$layanan_id = $_REQUEST['layanan_id'];
			$customer_id = $_REQUEST['customer_id'];
			$quantity = $_REQUEST['quantity'];
			$harga = $_REQUEST['harga'];
			$harga_total = $_REQUEST['harga_total'];
			$tunai = $_REQUEST['tunai'];
			$kembalian = $_REQUEST['kembalian'];
			$keterangan = $_REQUEST['keterangan'];

			$act = DB::INSERT((DB::raw("
				UPDATE `m_transaksi_customer` SET 
				`kode_transaksi`='".$kode_transaksi."',
				`tgl_pemesanan`='".$tgl_pemesanan."',
				`layanan_id`='".$layanan_id."',
				`customer_id`='".$customer_id."',
				`qty`='".$quantity."',
				`keterangan`='".$keterangan."',
				`harga`='".$harga."',
				`total`='".$harga_total."',
				`tunai`='".$tunai."',
				`kembalian`='".$kembalian."' WHERE `transaksi_id`='".$transaksi_id."'
				"))); 

			if ($act) {
				echo '
				<div class="alert alert-success">
				<strong>Perhatian !</strong> Data berhasil Disimpan
				</div>
				';
			}else{
				echo '
				<div class="alert alert-danger">
				<strong>Perhatian !</strong> Data gagal Disimpan
				</div>
				';
			}
		}

		?>
		<div class="card">
			<div class="card-header bg-primary">
				<h4 class="card-title" style="color: white">Transaksi Customer</h4>
			</div>
			<div class="card-body">
				<form action="" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Kode Transaksi</label>
						<input type="text" readonly="" class="form-control input-default" value="<?php echo $datcus[0]->kode_transaksi; ?>" name="kode_transaksi" placeholder="Tanggal Pemesanan">
					</div>

					<div class="form-group">
						<label>Tanggal Transaksi</label>
						<input type="date" class="form-control input-default" value="<?php echo $datcus[0]->tgl_pemesanan; ?>" name="tgl_pemesanan" placeholder="Tanggal Pemesanan">
					</div>

					<div class="form-group">
						<label>Pilih layanan</label>
						<select class="form-control" name="layanan_id" onchange="cek_harga()" id="layanan_id">
							<option value="">Pilih Layanan</option>
							<?php 
							$show_layanan = DB::SELECT(DB::RAW("SELECT * FROM `m_layanan` ORDER BY create_at ASC"));
							foreach ($show_layanan as $data):
								?>
								<option value="<?php echo $data->layanan_id; ?>"
									<?php echo ($data->layanan_id==$datcus[0]->layanan_id) ? 'selected': '';?>
								><?php echo $data->nama_layanan; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Harga</label>
						<input type="number" readonly="" id="harga"  class="form-control input-default" value="<?php echo $datcus[0]->harga; ?>" name="harga" placeholder="Harga">
					</div>
					<div class="form-group">
						<label>Quantity</label>
						<input type="number" onkeyup="total_harga()" id="quantity" class="form-control input-default" value="<?php echo $datcus[0]->qty; ?>" name="quantity" placeholder="Quantity">
					</div>
					<div class="form-group">
						<label>Total</label>
						<input type="number" readonly="" id="harga_total" class="form-control input-default" value="<?php echo $datcus[0]->total; ?>" name="harga_total" placeholder="Total">
					</div>
					<div class="form-group">
						<label>Pilih Customer</label>
						<select class="form-control" name="customer_id">
							<?php 
							$show_customer = DB::SELECT(DB::RAW("SELECT * FROM `m_customer` ORDER BY create_at ASC"));
							foreach ($show_customer as $data):
								?>
								<option value="<?php echo $data->customer_id; ?>"
									<?php echo ($data->customer_id==$datcus[0]->customer_id) ? 'selected': '';?>
								><?php echo strtoupper($data->nama_customer); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<input type="text" class="form-control input-default" value="<?php echo $datcus[0]->keterangan; ?>" name="keterangan" placeholder="Keterangan">
					</div>
					

				</div>
			</div>
		</div>
		<div class="col-xl-5 col-lg-5">
			<div class="card">
				<div class="card-header bg-primary">
					<h4 class="card-title" style="color: white">Pembayaran</h4>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label>Tunai</label>
						<input type="number" onkeyup="cash();"  id="tunai" value="<?php echo $datcus[0]->tunai; ?>" class="form-control input-default" name="tunai" placeholder="Tunai">
					</div>
					<div class="form-group">
						<label>Kembalian</label>
						<input type="number" id="kembalian" value="<?php echo $datcus[0]->kembalian; ?>" class="form-control input-default" name="kembalian" placeholder="Kembalian">
					</div>
					<div class="form-group">
						<a href="transaksi_customer" class="btn btn-danger mb-2">
							<i class="fa fa-arrow"></i> Kembali
						</a>
						<button type="submit" name="update" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	function cek_harga() {
		var layanan_id = $("#layanan_id").val();
		$.ajax({
			url: 'cek_harga_layanan?layanan_id='+layanan_id,
			type: 'GET',
			dataType: 'html',
			success : function(data) {
				$("#harga").val(data);
			}
		})
		
	}
	function total_harga() {
		var harga = parseInt($("#harga").val());
		var quantity = parseInt($("#quantity").val());
		$("#harga_total").val(harga*quantity);
		
	}

	function  cash() {
		var tunai = parseInt($("#tunai").val());
		var harga_total = parseInt($("#harga_total").val());
		$("#kembalian").val(tunai-harga_total);
	}


	
</script>

@endsection