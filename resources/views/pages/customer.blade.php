@extends('../main')
@section('container')

<div class="row">
    <div class="col-xl-12 col-lg-12">
        <?php 
                    if (isset($_REQUEST['simpan'])) {
                        $nama = $_REQUEST['nama'];
                        $hp = $_REQUEST['hp'];
                        $alamat = $_REQUEST['alamat'];

                        $act = DB::INSERT((DB::raw("INSERT INTO `m_customer`(
                           `kode`,
                           `nama_customer`,
                           `hp`,
                           `alamat`,
                           `create_at`) VALUES (
                           'KD-".date('Ymds')."',
                           '".$nama."',
                           '".$hp."',
                           '".$alamat."',
                           '".date('Y-m-d H:i:s')."')"))); 

                        if ($act) {
                            echo '
                            <div class="alert alert-success">
                            <strong>Perhatian !</strong> Data berhasil Disimpan
                            </div>
                            ';
                        }else{
                            echo '
                            <div class="alert alert-danger">
                            <strong>Perhatian !</strong> Data gagal Disimpan
                            </div>
                            ';
                        }
                    }

                    if (isset($_REQUEST['edit'])) {
                        $customer_id = $_REQUEST['customer_id'];
                        $nama = $_REQUEST['nama'];
                        $hp = $_REQUEST['hp'];
                        $alamat = $_REQUEST['alamat'];

                        $act = DB::UPDATE((DB::raw("

                            UPDATE `m_customer` SET 
                            `nama_customer`='".$nama."',
                            `hp`='".$hp."',
                            `alamat`='".$alamat."'
                            WHERE `customer_id`='".$customer_id."'

                            "))); 

                        if ($act) {
                            echo '
                            <div class="alert alert-success">
                            <strong>Perhatian !</strong> Data berhasil Disimpan
                            </div>
                            ';
                        }else{
                            echo '
                            <div class="alert alert-danger">
                            <strong>Perhatian !</strong> Data gagal Disimpan
                            </div>
                            ';
                        }
                    }

                     if (isset($_REQUEST['hapus'])) {
                        $customer_id = $_REQUEST['customer_id'];

                        $act = DB::DELETE((DB::raw("
                            DELETE FROM `m_customer` WHERE `customer_id`='".$customer_id."'
                            "))); 

                        if ($act) {
                            echo '
                            <div class="alert alert-success">
                            <strong>Perhatian !</strong> Data berhasil Dihapus
                            </div>
                            ';
                        }else{
                            echo '
                            <div class="alert alert-danger">
                            <strong>Perhatian !</strong> Data gagal Dihapus
                            </div>
                            ';
                        }
                    }


                    ?>

        <div class="card">
            <div class="card-header">
                <!-- <h4 class="card-title">Data Customer</h4> -->
                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#basicModal">
                    Tambah Customer
                </button>
            </div>
            <div class="card-body">
                
                <!-- Modal -->
                <div class="modal fade" id="basicModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Customer</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                                    <form action="" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control input-default" name="nama" placeholder="Input Nama">
                                        </div>
                                        <div class="form-group">
                                            <label>Hp</label>
                                            <input type="text" class="form-control" name="hp" placeholder="Nomor Hp">
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" rows="5" name="alamat" placeholder="Isi Alamat"></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="basic-form">
                    
                    <table class="display min-w850" id="example">
                        <thead>    
                            <tr>
                                <th width="1%">#</th>
                                <th width="10%">Kode</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Hp</th>
                                <th>Create At</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                            $no=1;
                            $show_cus = DB::SELECT(DB::RAW("SELECT * FROM m_customer ORDER BY create_at DESC"));
                            foreach ($show_cus as $dacus):
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>        
                                    <td><?php echo strtoupper($dacus->kode); ?></td>        
                                    <td><?php echo strtoupper($dacus->nama_customer) ?></td>        
                                    <td><?php echo $dacus->alamat ?></td>        
                                    <td><?php echo $dacus->hp ?></td> 
                                    <td><?php echo $dacus->create_at ?></td> 
                                    <td>
                                        <form action="" method="POST">
                                            {{ csrf_field() }}
                                            <a class="btn btn-primary"  data-toggle="modal" data-target="#edit<?php echo $dacus->customer_id; ?>"><i class="fa fa-pencil"></i></a>
                                            <input type="hidden" name="customer_id" value="<?php echo $dacus->customer_id; ?>">
                                          <button onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" class="btn btn-danger" type="submit" name="hapus"><i class="fa fa-trash"></i></button>
                                        </form>

                                            <div class="modal fade" id="edit<?php echo $dacus->customer_id; ?>">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Customer</h5>
                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="basic-form">
                                                                <form action="" method="POST">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="customer_id" value="<?php echo $dacus->customer_id; ?>">
                                                                    <div class="form-group">
                                                                        <label>Nama</label>
                                                                        <input type="text" class="form-control input-default" name="nama" placeholder="Input Nama" value="<?php echo $dacus->nama_customer; ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Hp</label>
                                                                        <input type="text" class="form-control" name="hp" placeholder="Nomor Hp" value="<?php echo $dacus->hp; ?>">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Alamat</label>
                                                                        <textarea class="form-control input-default" rows="5" name="alamat" placeholder="Isi Alamat"><?php echo $dacus->alamat ?></textarea>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                                                <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </td>       
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection