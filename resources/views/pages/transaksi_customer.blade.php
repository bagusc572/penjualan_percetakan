@extends('../main')
@section('container')

<?php 

if (isset($_REQUEST['hapus'])) {
	$transaksi_id = $_REQUEST['transaksi_id'];

	$act = DB::DELETE((DB::raw("
		DELETE FROM `m_transaksi_customer` WHERE `transaksi_id`='".$transaksi_id."'
		"))); 

	if ($act) {
		echo '
		<div class="alert alert-success">
		<strong>Perhatian !</strong> Data berhasil Dihapus
		</div>
		';
	}else{
		echo '
		<div class="alert alert-danger">
		<strong>Perhatian !</strong> Data gagal Dihapus
		</div>
		';
	}
}

  ?>
<div class="row">
	<div class="col-xl-12 col-lg-12">
		<div class="card">
			<div class="card-header">
				<!-- <h4 class="card-title">Data Customer</h4> -->
				<a href="add_transaksi_customer_cart" class="btn btn-primary mb-2">
					<i class="fa fa-shopping-cart"></i> Tambah transaksi customer
				</a>
			</div>
			<div class="card-body">
				<table class="display min-w850" id="example">
					<thead>    
						<tr>
							<th width="1%">#</th>
							<th width="10%">Kode Transaksi</th>
							<th width="10%">Tanggal</th>
							<th>Barang</th>
							<th>customer</th>
							<th>Harga</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Tunai</th>
							<th>Kembalian</th>
							<th>status</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$no=1;
						$show_transaksi_customer = DB::SELECT(DB::RAW("
							SELECT * FROM `m_transaksi_customer` AS a
							LEFT JOIN m_layanan as b ON a.layanan_id = b.layanan_id
							LEFT JOIN m_customer as c ON a.customer_id = c.customer_id
							ORDER BY a.create_at DESC
						"));
						foreach ($show_transaksi_customer as $datransaksi_customer):
							?>
							<tr>
								<td><?php echo $no++; ?></td>        
								<td><?php echo $datransaksi_customer->kode; ?></td>   
								<td><?php echo $datransaksi_customer->tgl_pemesanan; ?></td>   
								<td><?php echo $datransaksi_customer->nama_layanan; ?></td>   
								<td><?php echo $datransaksi_customer->nama_customer; ?></td>   
								<td><?php echo number_format($datransaksi_customer->harga); ?></td>   
								<td><?php echo $datransaksi_customer->qty; ?></td>   
								<td style="text-align: center;"><?php echo number_format($datransaksi_customer->total); ?></td>   
								<td style="text-align: center;"><?php echo number_format($datransaksi_customer->tunai); ?></td>   
								<td style="text-align: center;"><?php echo number_format($datransaksi_customer->kembalian); ?></td>   
								<td><?php echo $datransaksi_customer->status_pesanan; ?></td>   
								<td nowrap="">
									<form action="" method="POST">
										{{ csrf_field() }}
										<a href="edit_transaksi_customer?id_transaksi=<?php echo $datransaksi_customer->transaksi_id; ?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>

										<input type="hidden" name="transaksi_id" value="<?php echo $datransaksi_customer->transaksi_id; ?>">
										<button onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" class="btn btn-danger btn-sm" type="submit" name="hapus"><i class="fa fa-trash"></i></button>
									</form>
									
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection