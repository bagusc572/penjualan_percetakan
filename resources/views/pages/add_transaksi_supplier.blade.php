@extends('../main')
@section('container')
<style type="text/css">
	.form-control{
		color: black;
	}
</style>
<div class="row">
	<div class="col-xl-12 col-lg-12">
		<?php 

		 if (isset($_REQUEST['simpan'])) {
            $tgl_pemesanan = $_REQUEST['tgl_pemesanan'];
            $barang_id = $_REQUEST['barang_id'];
            $supplier_id = $_REQUEST['supplier_id'];
            $quantity = $_REQUEST['quantity'];
            $harga_beli = $_REQUEST['harga_beli'];
            $keterangan = $_REQUEST['keterangan'];

            $act = DB::INSERT((DB::raw("
            	INSERT INTO `m_transaksi_supplier`(`tgl_pemesanan`, `barang_id`, `supplier_id`, `qty`, `keterangan`, `create_at`, `harga_beli`) VALUES (
            	'".$tgl_pemesanan."',
            	'".$barang_id."',
            	'".$supplier_id."',
            	'".$quantity."',
            	'".$keterangan."',
            	'".date('Y-m-d H:i:s')."',
            	'".$harga_beli."')
            "))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Disimpan
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Disimpan
                </div>
                ';
            }
        }

        ?>
		<div class="card">
			<div class="card-header">
				<!-- <h4 class="card-title">Data Customer</h4> -->
				<a href="transaksi_supplier" class="btn btn-danger mb-2">
					<i class="fa fa-arrow"></i> Kembali
				</a>
			</div>
			<div class="card-body">
				<form action="" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Tanggal Transaksi</label>
						<input type="date" class="form-control input-default" value="<?php echo date('Y-m-d	'); ?>" name="tgl_pemesanan" placeholder="Tanggal Pemesanan">
					</div>

					<div class="form-group">
						<label>Pilih Barang</label>
						<select class="form-control" name="barang_id">
						<?php 
							$show_barang = DB::SELECT(DB::RAW("SELECT * FROM `m_barang` ORDER BY create_at ASC"));
							foreach ($show_barang as $data):
						?>
							<option value="<?php echo $data->barang_id; ?>"><?php echo $data->nama_barang; ?></option>
						<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Pilih Supplier</label>
						<select class="form-control" name="supplier_id">
						<?php 
							$show_supplier = DB::SELECT(DB::RAW("SELECT * FROM `m_supplier` ORDER BY create_at ASC"));
							foreach ($show_supplier as $data):
						?>
							<option value="<?php echo $data->supplier_id; ?>"><?php echo $data->nama_supplier; ?></option>
						<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<label>Quantity</label>
						<input type="number" class="form-control input-default" value="" name="quantity" placeholder="Quantity">
					</div>
					<div class="form-group">
						<label>Harga Beli</label>
						<input type="number" class="form-control input-default" value="" name="harga_beli" placeholder="Harga Beli">
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<input type="text" class="form-control input-default" value="" name="keterangan" placeholder="Keterangan">
					</div>
					<div class="form-group">
						<button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection