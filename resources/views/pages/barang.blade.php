@extends('../main')
@section('container')

<div class="row">
    <div class="col-xl-12 col-lg-12">
        <?php 
        if (isset($_REQUEST['simpan'])) {
            $kode_barang = $_REQUEST['kode_barang'];
            $nama_barang = $_REQUEST['nama_barang'];

            $act = DB::INSERT((DB::raw("INSERT INTO `m_barang`(`kode`, `nama_barang`, `create_at`) 
                VALUES (
                '".$kode_barang."',
                '".$nama_barang."',
                '".date('Y-m-d H:i:s')."')"))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Disimpan
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Disimpan
                </div>
                ';
            }
        }

        if (isset($_REQUEST['edit'])) {
            $barang_id = $_REQUEST['barang_id'];
            $kode_barang = $_REQUEST['kode_barang'];
            $nama_barang = $_REQUEST['nama_barang'];

            $act = DB::UPDATE((DB::raw("

                UPDATE `m_barang` SET 
                `kode`='".$kode_barang."',
                `nama_barang`='".$nama_barang."'
                WHERE `barang_id`='".$barang_id."'

                "))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Disimpan
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Disimpan
                </div>
                ';
            }
        }

        if (isset($_REQUEST['hapus'])) {
            $barang_id = $_REQUEST['barang_id'];

            $act = DB::DELETE((DB::raw("
                DELETE FROM `m_barang` WHERE `barang_id`='".$barang_id."'
                "))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Dihapus
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Dihapus
                </div>
                ';
            }
        }


        ?>

        <div class="card">
            <div class="card-header">
                <!-- <h4 class="card-title">Data Customer</h4> -->
                <button type="button" class="btn btn-success mb-2" data-toggle="modal" data-target="#basicModal">
                    Tambah Barang
                </button>
            </div>
            <div class="card-body">

                <!-- Modal -->
                <div class="modal fade" id="basicModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Barang</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                                    <form action="" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Kode </label>
                                            <input type="text" class="form-control" name="kode_barang" placeholder="Kode Barang">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input type="text" class="form-control input-default" name="nama_barang" placeholder="Input Nama Barang">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="basic-form">

                    <table class="display min-w850" id="example">
                        <thead>    
                            <tr>
                                <th width="1%">#</th>
                                <th width="10%">Kode</th>
                                <th>Nama Barang</th>
                                <th>Create At</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                            $no=1;
                            $show_barang = DB::SELECT(DB::RAW("SELECT * FROM m_barang ORDER BY create_at DESC"));
                            foreach ($show_barang as $dabarang):
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>        
                                    <td><?php echo strtoupper($dabarang->kode); ?></td>        
                                    <td><?php echo strtoupper($dabarang->nama_barang) ?></td>   
                                    <td><?php echo $dabarang->create_at ?></td> 
                                    <td>
                                        <form action="" method="POST">
                                            {{ csrf_field() }}
                                            <a class="btn btn-primary"  data-toggle="modal" data-target="#edit<?php echo $dabarang->barang_id; ?>"><i class="fa fa-pencil"></i></a>
                                            <input type="hidden" name="barang_id" value="<?php echo $dabarang->barang_id; ?>">
                                            <button onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" class="btn btn-danger" type="submit" name="hapus"><i class="fa fa-trash"></i></button>
                                        </form>

                                        <div class="modal fade" id="edit<?php echo $dabarang->barang_id; ?>">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit Customer</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="basic-form">
                                                            <form action="" method="POST">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="barang_id" value="<?php echo $dabarang->barang_id; ?>">
                                                                <div class="form-group">
                                                                    <label>Kode</label>
                                                                    <input type="text" class="form-control input-default" name="kode_barang" placeholder="Input Nama" value="<?php echo $dabarang->kode; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Nama Barang</label>
                                                                    <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" value="<?php echo $dabarang->nama_barang; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                                            <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>       
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection