@extends('../main')
@section('container')



<div class="row">
    <div class="col-xl-12 col-lg-12">
        <?php 
        if (isset($_REQUEST['simpan'])) {
            $kode = $_REQUEST['kode'];
            $nama_layanan = $_REQUEST['nama_layanan'];
            $harga = $_REQUEST['harga'];

            $act = DB::INSERT((DB::raw("
            		INSERT INTO `m_layanan`(`kode`, `nama_layanan`, `harga`, `create_at`) VALUES (
            		'".$kode."',
            		'".$nama_layanan."',
            		'".$harga."',
            		'".date('Y-m-d H:i:s')."')
            	"))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Disimpan
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Disimpan
                </div>
                ';
            }
        }

        if (isset($_REQUEST['edit'])) {
            $layanan_id = $_REQUEST['layanan_id'];
            $kode_layanan = $_REQUEST['kode_layanan'];
            $nama_layanan = $_REQUEST['nama_layanan'];

            $act = DB::UPDATE((DB::raw("

                UPDATE `m_layanan` SET 
                `kode`='".$kode_layanan."',
                `nama_layanan`='".$nama_layanan."'
                WHERE `layanan_id`='".$layanan_id."'

                "))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Disimpan
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Disimpan
                </div>
                ';
            }
        }

        if (isset($_REQUEST['hapus'])) {
            $layanan_id = $_REQUEST['layanan_id'];

            $act = DB::DELETE((DB::raw("
                DELETE FROM `m_layanan` WHERE `layanan_id`='".$layanan_id."'
                "))); 

            if ($act) {
                echo '
                <div class="alert alert-success">
                <strong>Perhatian !</strong> Data berhasil Dihapus
                </div>
                ';
            }else{
                echo '
                <div class="alert alert-danger">
                <strong>Perhatian !</strong> Data gagal Dihapus
                </div>
                ';
            }
        }


        ?>

        <div class="card">
            <div class="card-header">
                <!-- <h4 class="card-title">Data Customer</h4> -->
                <button type="button" class="btn btn-success mb-2" data-toggle="modal" data-target="#basicModal">
                    Tambah Layanan
                </button>
            </div>
            <div class="card-body">

                <!-- Modal -->
                <div class="modal fade" id="basicModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Tambah Layanan</h5>
                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                                    <form action="" method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Kode Layanan</label>
                                            <input type="text" class="form-control" name="kode" placeholder="Kode Layanan">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Layanan</label>
                                            <input type="text" class="form-control input-default" name="nama_layanan" placeholder="Input Nama Layanan">
                                        </div>
                                         <div class="form-group">
                                            <label>Harga</label>
                                            <input type="number" class="form-control input-default" name="harga" placeholder="Input Harga">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="basic-form">

                    <table class="display min-w850" id="example">
                        <thead>    
                            <tr>
                                <th width="1%">#</th>
                                <th width="10%">Kode</th>
                                <th>Nama Layanan</th>
                                <th>Harga</th>
                                <th>Create At</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                            $no=1;
                            $show_layanan = DB::SELECT(DB::RAW("SELECT * FROM `m_layanan` ORDER BY create_at DESC"));
                            foreach ($show_layanan as $dalayanan):
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>        
                                    <td><?php echo strtoupper($dalayanan->kode); ?></td>        
                                    <td><?php echo strtoupper($dalayanan->nama_layanan) ?></td>   
                                    <td><?php echo number_format($dalayanan->harga) ?></td>   
                                    <td><?php echo $dalayanan->create_at ?></td> 
                                    <td>
                                        <form action="" method="POST">
                                            {{ csrf_field() }}
                                            <a class="btn btn-primary"  data-toggle="modal" data-target="#edit<?php echo $dalayanan->layanan_id; ?>"><i class="fa fa-pencil"></i></a>
                                            <input type="hidden" name="layanan_id" value="<?php echo $dalayanan->layanan_id; ?>">
                                            <button onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" class="btn btn-danger" type="submit" name="hapus"><i class="fa fa-trash"></i></button>
                                        </form>

                                        <div class="modal fade" id="edit<?php echo $dalayanan->layanan_id; ?>">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit Customer</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="basic-form">
                                                            <form action="" method="POST">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="layanan_id" value="<?php echo $dalayanan->layanan_id; ?>">
                                                                <div class="form-group">
                                                                    <label>Kode</label>
                                                                    <input type="text" class="form-control input-default" name="kode_layanan" placeholder="Input Nama" value="<?php echo $dalayanan->kode; ?>">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Nama layanan</label>
                                                                    <input type="text" class="form-control" name="nama_layanan" placeholder="Nama layanan" value="<?php echo $dalayanan->nama_layanan; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger light" data-dismiss="modal">Tutup</button>
                                                            <button type="submit" name="edit" class="btn btn-primary">Simpan</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>       
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection