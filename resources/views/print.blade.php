<!DOCTYPE html>
<html>
<head>
	<title>Print Transaksi</title>
	<style type="text/css">
		table {
			border-collapse: collapse;
			padding: 50px;
		}
		table tr th {
			text-align: left;	
			padding: 10px
		}
		table tr td {
			text-align: left;	
			padding: 10px
		}
		body{
			font-family: 'arial'
		}
	</style>
</head>
<body onload="print()">
	<h1>Form Bukti Pemesanan</h1>
	<?php 
	$show_customer = DB::SELECT(DB::RAW("
		SELECT * FROM `m_transaksi_customer` AS a
		LEFT JOIN m_layanan as b ON a.layanan_id = b.layanan_id
		LEFT JOIN m_customer as c ON a.customer_id = c.customer_id
		WHERE a.kode_transaksi='".$_REQUEST['kode_transaksi']."'
		ORDER BY a.create_at DESC"));
		?>
		<table border="1px" style="width: 100%">
			<tr>
				<th>Kode Transaksi</th>
				<th>: <?php echo $show_customer[0]->kode_transaksi; ?></th>
			</tr>
			<tr>
				<th>Tanggal Pemesanan</th>
				<th>: <?php echo $show_customer[0]->tgl_pemesanan; ?></th>
			</tr>
			<tr>
				<th>Nama</th>
				<th>: <?php echo $show_customer[0]->nama_customer; ?></th>
			</tr>
			<tr>
				<th>Status Pembayaran</th>
				<th>: <?php echo $show_customer[0]->status_pembayaran; ?></th>
			</tr>
		</table>

		<table style="width: 100%" border="1px">
			<tr style="background-color: grey; color: white">
				<th>#</th>
				<th>Layanan</th>
				<th>Harga</th>
				<th>Qty</th>
				<th>Total</th>
			</tr>
			<?php 
			$no=1;
			foreach ($show_customer as $data):?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $data->nama_layanan; ?></td>
					<td><?php echo $data->harga; ?></td>
					<td><?php echo $data->qty; ?></td>
					<td><?php echo number_format($data->qty*$data->harga); ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td colspan="4">Total</td>
				<td><?php echo number_format($show_customer[0]->total); ?></td>
			</tr>
		</table>
	</body>
	</html>