<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from azmind.com/premium/marco/v2-1/layout-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2016 02:21:16 GMT -->
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Omah Banner</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">        
        <link rel="stylesheet" href="{{ asset('login/assets/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('login/assets/typicons/typicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('login/assets/css/animate.css') }}">
		  <link rel="stylesheet" href="{{ asset('login/assets/css/form-elements.css') }}">
        <link rel="stylesheet" href="{{ asset('login/assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('login/assets/css/media-queries.css') }}">
    </head>

    <body>
    
        <!-- Loader -->
    	{{-- <div class="loader">
    		<div class="loader-img"></div>
    	</div> --}}
				
        <!-- Top content -->
        <div class="top-content">
        	
        	<!-- Top menu -->
			<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="">Omah-Banner</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="top-navbar-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a class="btn btn-link-2" href="masuk">Login</a></li>
						</ul>
					</div>
				</div>
			</nav>
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text">
                            <h1 class="wow fadeInLeftBig">Omah <strong>Banner</strong></h1>
                            <div class="description wow fadeInLeftBig">
                            	<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid adipisci pariatur commodi tempore natus minima, possimus doloremque vero molestias. Sint cumque eius itaque maiores magnam quis fugit. Excepturi, totam ut!</p>
                            </div>
                        </div>
                        <div class="col-sm-5 form-box wow fadeInUp">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Cek Pesanan</h3>
                            		<p>Anda bisa cek pesanan disini :</p>
                        		</div>
                        		<div class="form-top-right">
                        			<span aria-hidden="true" class="typcn typcn-pencil"></span>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form  action="" method="post">
                                    {{ @csrf_field() }}
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-first-name">Kode Pesanan</label>
			                        	<input type="text" value="<?php echo @$_REQUEST['kode']; ?>" name="kode" placeholder="Input kode pesanan disini" class="form-first-name form-control">
			                        </div>
			                        <button type="submit" class="btn" name="cek">Cek</button>
			                    </form>

                                <?php if(isset($_REQUEST['cek'])): 
                                    $cekdata = DB::SELECT(DB::RAW("SELECT * FROM `m_transaksi_customer` as a
                                        LEFT JOIN m_customer as b ON a.customer_id = b.customer_id
                                        LEFT JOIN m_layanan as c ON a.layanan_id = c.layanan_id
                                     WHERE a.kode_transaksi='".$_REQUEST['kode']."'"));
                                ?>
                                    <br>
                                    <div class="alert alert-danger">
                                        <label style="font-weight: bold;">Status Pesanan Anda</label>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td width="1%">Tanggal</td>
                                                <td>: <b><?php echo $cekdata[0]->tgl_pemesanan; ?></b></td>
                                            </tr>
                                            <tr>
                                                <td width="1%">Kode</td>
                                                <td>: <b><?php echo $cekdata[0]->kode_transaksi; ?></b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama</td>
                                                <td>: <b><?php echo $cekdata[0]->nama_customer; ?></b></td>
                                            </tr>
                                            <tr>
                                                <td>Pesanan</td>
                                                <td>: <b><?php echo $cekdata[0]->nama_layanan; ?></b></td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>: <b><?php echo $cekdata[0]->status_pesanan; ?></b></td>
                                            </tr>
                                        </table>
                                    </div>
                                <?php endif; ?>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>


        <!-- Javascript -->
        <script src="{{ asset('login/assets/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{ asset('login/assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('login/assets/js/jquery.backstretch.min.js')}}"></script>
        <script src="{{ asset('login/assets/js/wow.min.js')}}"></script>
        <script src="{{ asset('login/assets/js/retina-1.1.0.min.js')}}"></script>
        <script src="{{ asset('login/assets/js/scripts.js')}}"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>


<!-- Mirrored from azmind.com/premium/marco/v2-1/layout-1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 May 2016 02:22:08 GMT -->
</html>