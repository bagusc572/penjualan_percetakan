<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/','Main@index')->name('index');
Route::match(['get', 'post'], '/masuk','Main@masuk')->name('masuk');
Route::match(['get', 'post'], '/logout','Main@logout')->name('logout');
Route::match(['get', 'post'], '/login_act','Action@login_act')->name('login_act');
Route::match(['get', 'post'], '/dashboard','Main@dashboard')->name('dashboard');
Route::match(['get', 'post'], '/customer','Main@customer')->name('customer');
Route::match(['get', 'post'], '/supplier','Main@supplier')->name('supplier');
Route::match(['get', 'post'], '/barang','Main@barang')->name('barang');
Route::match(['get', 'post'], '/transaksi_supplier','Main@transaksi_supplier')->name('transaksi_supplier');
Route::match(['get', 'post'], '/add_transaksi_supplier','Main@add_transaksi_supplier')->name('add_transaksi_supplier');
Route::match(['get', 'post'], '/layanan','Main@layanan')->name('layanan');
Route::match(['get', 'post'], '/transaksi_customer','Main@transaksi_customer')->name('transaksi_customer');
Route::match(['get', 'post'], '/add_transaksi_customer','Main@add_transaksi_customer')->name('add_transaksi_customer');
Route::match(['get', 'post'], '/cek_harga_layanan','Main@cek_harga_layanan')->name('cek_harga_layanan');
Route::match(['get', 'post'], '/edit_transaksi_supplier','Main@edit_transaksi_supplier')->name('edit_transaksi_supplier');
Route::match(['get', 'post'], '/edit_transaksi_customer','Main@edit_transaksi_customer')->name('edit_transaksi_customer');
Route::match(['get', 'post'], '/transaksi_baru','Main@transaksi_baru')->name('transaksi_baru');
Route::match(['get', 'post'], '/transaksi_proses','Main@transaksi_proses')->name('transaksi_proses');
Route::match(['get', 'post'], '/transaksi_selesai','Main@transaksi_selesai')->name('transaksi_selesai');
Route::match(['get', 'post'], '/report_perbulan','Main@report_perbulan')->name('report_perbulan');
Route::match(['get', 'post'], '/add_transaksi_customer_cart','Main@add_transaksi_customer_cart')->name('add_transaksi_customer_cart');
Route::match(['get', 'post'], '/print','Main@print')->name('print');