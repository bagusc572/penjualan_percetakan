<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Request;
use Session;
use Redirect;

class Action extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function login_act(){
        $username = @$_REQUEST['username'];
        $password = md5(@$_REQUEST['password']);
        
        $users_count = DB::table('m_admin')
            ->where('username', '=', $username)
            ->where('password', '=', $password)
            ->count();
        if ($users_count>=1) {
            Session::put('username', 'password');
            Session::put('level', 'level');
            return Redirect::route('dashboard');
        }else{
            return view('masuk');
        }
    }

}