<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Request;
use Session;
use Redirect;

class Main extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function cek_login()
    {
        $cek_session = Session::get('level');
        if(empty($cek_session)){
            echo  "
                <script>
                    document.location='masuk';
                </script>
            ";
        }else{

        }   
    }

    public function index()
    {
        return view('front');
    }
    public function dashboard()
    {
        $this->cek_login();
        return view('pages.home');
    }
    
    public function masuk()
    {
        $cek_session = Session::get('level');

        if(!empty($cek_session)){
            return Redirect::route('dashboard');
        }else{
            return view('masuk');
        }   
    }

    public function customer()
    {
        $this->cek_login();
        return view('pages.customer');
    }
    public function supplier()
    {
        $this->cek_login();
        return view('pages.supplier');
    }
    public function barang()
    {
        $this->cek_login();
        return view('pages.barang');
    }
    public function transaksi_supplier()
    {
        $this->cek_login();
        return view('pages.transaksi_supplier');
    }
    public function add_transaksi_supplier()
    {
        $this->cek_login();
        return view('pages.add_transaksi_supplier');
    }
    public function layanan()
    {
        $this->cek_login();
        return view('pages.layanan');
    }
    public function transaksi_customer()
    {
        $this->cek_login();
        return view('pages.transaksi_customer');
    } 
    public function add_transaksi_customer()
    {
        $this->cek_login();
        return view('pages.add_transaksi_customer');
    }
    public function cek_harga_layanan()
    {
        $this->cek_login();
        return view('pages.cek_harga_layanan');
    }
    public function edit_transaksi_supplier()
    {
        $this->cek_login();
        return view('pages.edit_transaksi_supplier');
    }
    public function edit_transaksi_customer()
    {
        $this->cek_login();
        return view('pages.edit_transaksi_customer');
    } 

    public function transaksi_baru()
    {
        return view('pages.transaksi_baru');
    }
    public function transaksi_proses()
    {
        return view('pages.transaksi_proses');
    }
     public function transaksi_selesai()
    {
        return view('pages.transaksi_selesai');
    }
    public function report_perbulan()
    {
        return view('pages.report_perbulan');
    }
    public function add_transaksi_customer_cart()
    {
        return view('pages.add_transaksi_customer_cart');
    }
    function print(){
        return view('print');
    }

    public function logout()
    {
        Session::flush();
        return Redirect::route('masuk');
    }
}