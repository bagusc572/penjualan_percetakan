-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jul 2022 pada 21.01
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualan_percetakan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_admin`
--

CREATE TABLE `m_admin` (
  `admin_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('1','2') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_admin`
--

INSERT INTO `m_admin` (`admin_id`, `nama`, `username`, `password`, `level`) VALUES
(4, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_barang`
--

CREATE TABLE `m_barang` (
  `barang_id` int(11) NOT NULL,
  `kode` text NOT NULL,
  `nama_barang` text NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_barang`
--

INSERT INTO `m_barang` (`barang_id`, `kode`, `nama_barang`, `create_at`) VALUES
(1, '1', 'Banner', '2022-06-26 00:46:32'),
(2, '2', 'Tinta', '2022-06-26 00:51:19'),
(3, '3', 'Sablon', '2022-06-26 00:51:48'),
(5, '5', 'Frame Border Batik', '2022-06-26 01:51:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_customer`
--

CREATE TABLE `m_customer` (
  `customer_id` int(11) NOT NULL,
  `kode` text NOT NULL,
  `nama_customer` text NOT NULL,
  `hp` text NOT NULL,
  `alamat` text NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_customer`
--

INSERT INTO `m_customer` (`customer_id`, `kode`, `nama_customer`, `hp`, `alamat`, `create_at`) VALUES
(2, 'KD-2022062542', 'HIDAYATURROBBY DIANTORO', '02992939', 'Alamat', '2022-06-25 13:37:42'),
(3, 'KD-2022062526', 'andi', '081237799942', 'Jombang', '2022-06-25 13:43:26'),
(4, 'KD-2022062532', 'FAUZIE MUSTAQIEM YOS SE, MM', '02992939', 'Jombang', '2022-06-25 23:21:32'),
(5, 'KD-2022062517', 'Muhammad Haqi', '081237799942', 'Jombang', '2022-06-25 23:23:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_layanan`
--

CREATE TABLE `m_layanan` (
  `layanan_id` int(11) NOT NULL,
  `kode` text NOT NULL,
  `nama_layanan` text NOT NULL,
  `harga` text NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_layanan`
--

INSERT INTO `m_layanan` (`layanan_id`, `kode`, `nama_layanan`, `harga`, `create_at`) VALUES
(1, '1', 'Paket cetak 10 baner', '1000000', '2022-06-26 02:16:11'),
(2, '2', 'Cetak banner 1x2 M ', '100000', '2022-06-26 02:19:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_supplier`
--

CREATE TABLE `m_supplier` (
  `supplier_id` int(11) NOT NULL,
  `kode` text NOT NULL,
  `nama_supplier` text NOT NULL,
  `hp` text NOT NULL,
  `alamat` text NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_supplier`
--

INSERT INTO `m_supplier` (`supplier_id`, `kode`, `nama_supplier`, `hp`, `alamat`, `create_at`) VALUES
(8, 'SP-2022062520', 'PT Argowisata', '081237799942', 'Jombang', '2022-06-25 23:34:20'),
(9, 'SP-2022062608', 'CV Wika', '038849958', 'Alamat\r\n', '2022-06-26 01:52:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_transaksi_customer`
--

CREATE TABLE `m_transaksi_customer` (
  `transaksi_id` int(11) NOT NULL,
  `kode_transaksi` text NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `layanan_id` int(24) NOT NULL,
  `customer_id` int(24) NOT NULL,
  `qty` double NOT NULL,
  `keterangan` text NOT NULL,
  `create_at` datetime NOT NULL,
  `harga` int(11) NOT NULL,
  `total` int(100) NOT NULL,
  `tunai` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL,
  `status_pesanan` varchar(100) NOT NULL,
  `status_pembayaran` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_transaksi_customer`
--

INSERT INTO `m_transaksi_customer` (`transaksi_id`, `kode_transaksi`, `tgl_pemesanan`, `layanan_id`, `customer_id`, `qty`, `keterangan`, `create_at`, `harga`, `total`, `tunai`, `kembalian`, `status_pesanan`, `status_pembayaran`) VALUES
(7, 'KD20220701065203', '2022-07-01', 1, 2, 5, 'ok', '2022-07-01 18:52:21', 1000000, 5100000, 10000000, 4900000, 'baru', 'Lunas'),
(8, 'KD20220701065203', '2022-07-01', 2, 2, 1, 'ok', '2022-07-01 18:52:21', 100000, 5100000, 10000000, 4900000, 'baru', 'Lunas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_transaksi_customer_temporary`
--

CREATE TABLE `m_transaksi_customer_temporary` (
  `id_temporari` int(11) NOT NULL,
  `layanan_id` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_transaksi_supplier`
--

CREATE TABLE `m_transaksi_supplier` (
  `transaksi_id` int(11) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `barang_id` int(24) NOT NULL,
  `supplier_id` int(24) NOT NULL,
  `qty` double NOT NULL,
  `keterangan` text NOT NULL,
  `create_at` datetime NOT NULL,
  `harga_beli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_transaksi_supplier`
--

INSERT INTO `m_transaksi_supplier` (`transaksi_id`, `tgl_pemesanan`, `barang_id`, `supplier_id`, `qty`, `keterangan`, `create_at`, `harga_beli`) VALUES
(1, '2022-06-26', 1, 8, 10, 'keterangan', '2022-06-26 00:00:00', 10000),
(2, '2022-06-27', 3, 8, 200, 'Keterangan', '2022-06-26 01:46:58', 10000),
(3, '2022-06-27', 2, 8, 900, 'keterangan', '2022-06-26 04:28:56', 10000),
(4, '2022-06-26', 3, 9, 1000, 'keterangan', '2022-06-26 04:26:41', 200000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_admin`
--
ALTER TABLE `m_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `m_barang`
--
ALTER TABLE `m_barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- Indexes for table `m_customer`
--
ALTER TABLE `m_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `m_layanan`
--
ALTER TABLE `m_layanan`
  ADD PRIMARY KEY (`layanan_id`);

--
-- Indexes for table `m_supplier`
--
ALTER TABLE `m_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `m_transaksi_customer`
--
ALTER TABLE `m_transaksi_customer`
  ADD PRIMARY KEY (`transaksi_id`);

--
-- Indexes for table `m_transaksi_customer_temporary`
--
ALTER TABLE `m_transaksi_customer_temporary`
  ADD PRIMARY KEY (`id_temporari`);

--
-- Indexes for table `m_transaksi_supplier`
--
ALTER TABLE `m_transaksi_supplier`
  ADD PRIMARY KEY (`transaksi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_barang`
--
ALTER TABLE `m_barang`
  MODIFY `barang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_customer`
--
ALTER TABLE `m_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_layanan`
--
ALTER TABLE `m_layanan`
  MODIFY `layanan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_supplier`
--
ALTER TABLE `m_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `m_transaksi_customer`
--
ALTER TABLE `m_transaksi_customer`
  MODIFY `transaksi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `m_transaksi_customer_temporary`
--
ALTER TABLE `m_transaksi_customer_temporary`
  MODIFY `id_temporari` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `m_transaksi_supplier`
--
ALTER TABLE `m_transaksi_supplier`
  MODIFY `transaksi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
